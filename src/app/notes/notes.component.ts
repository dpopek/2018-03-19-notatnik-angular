import {Component, OnInit} from '@angular/core';
import {NotesService} from '../notes.service';
import {Note} from '../note';

@Component({
  selector: 'app-notes-list',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})

export class NotesComponent {

  constructor(public notesService: NotesService) {
  }

  selectNote(note: Note) {
    this.notesService.selectedNote = note;
  }
}
