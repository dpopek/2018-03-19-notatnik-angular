import {Injectable} from '@angular/core';
import {Note} from './note';

@Injectable({
  providedIn: 'root'
})

export class NotesService {

  private notesList: Note[] = [];

  constructor() {
  }

  selectedNote: Note = {name: '', text: ''};

  add(note: Note): void {
    this.notesList.push(note);
  }

  delete(): void {
    this.notesList = this.notesList.filter(note => note !== this.selectedNote);
  }

  get(): Note[] {
    return this.notesList;
  }
}
