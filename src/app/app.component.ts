import {Component} from '@angular/core';
import {NotesService} from './notes.service';
import {Note} from './note';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(public notesService: NotesService) {
  }

  addNote(name: string, text: string) {

    const note: Note = {
      name,
      text
    };

    this.notesService.add(note);
  }

  deleteNote() {
    this.notesService.delete();

  }
}
